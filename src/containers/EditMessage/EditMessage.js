import React, {Component} from 'react';
import MessageForm from "../../components/MessageForm/MessageForm";
import axios from "../../axios-blog";

class EditMessage extends Component {
  editMessage = message => {
    const id = this.props.match.params.id;
    axios.put('messages/' + id + '.json', message).then(() => {
      this.props.history.replace('/');
    })
  };

  render() {
    return (
      <div>
        <MessageForm
          messageId={this.props.match.params.id}
          heading="Edit message"
          submit={this.editMessage}
        />
      </div>
    );
  }
}

export default EditMessage;