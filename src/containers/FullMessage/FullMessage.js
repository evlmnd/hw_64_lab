import React, {Component} from 'react';
import axios from '../../axios-blog';
import './FullMessage.css'

class FullMessage extends Component {

  state = {};

  editClick = (event) => {
    event.preventDefault();

    this.props.history.push({
      pathname: this.props.match.params.id + '/edit'
    })
  };

  removeClick = (event) => {
    event.preventDefault();


    axios.delete('messages/' + this.props.match.params.id + '.json').catch(error => {
      console.log(error);
    }).finally(() => {
      this.props.history.push('/');
    });
  };

  componentDidMount() {
    axios.get('messages/' + this.props.match.params.id + '.json').then(response => {
      this.setState({message: response.data});
    }).catch(error => {
      console.log(error);
    });
  }

  render() {

    const message = this.state.message;
    let messageBlock = null;

    if (message) {
      messageBlock = (
        <div>
          <h4>{message.title}</h4>
          <p className="date">{message.date.slice(0, 16).replace('T', ' ')}</p>
          <p className="description">{message.description}</p>
          <button onClick={this.editClick}>Edit</button>
          <button onClick={this.removeClick}>Remove</button>
        </div>
      );
    }

    return (
      <div className="message-show">
        {messageBlock}
      </div>
    );
  }
}

export default FullMessage;