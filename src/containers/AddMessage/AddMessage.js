import React, {Component} from 'react';
import axios from '../../axios-blog';

import './AddMessage.css';
import MessageForm from "../../components/MessageForm/MessageForm";


class AddMessage extends Component {

  saveMessage = message => {
    axios.post('/messages.json', message).finally(() => {
      this.props.history.push('/');
    })

  };

  render() {
    return (
      <div>
        <MessageForm
          heading="Add message"
          submit={this.saveMessage}
        />
      </div>
    );
  }
}

export default AddMessage;