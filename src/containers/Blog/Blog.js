import React, {Component} from 'react';
import axios from '../../axios-blog';

import './Blog.css';
import Message from "../../components/Message/Message";

class Blog extends Component {

  state = {
    messages: {}
  };

  readMore = event => {
    event.preventDefault();

    const id = event.target.id;
    console.log(id);

    this.props.history.push({
      pathname: '/messages/' + id
    })

  };


  componentDidMount() {
    axios.get('/messages.json').then(response => {
      this.setState({messages: response.data});
    }).catch(error => {
      console.log(error);
    });
  }

  render() {

    let messages = [];

    for (const key in this.state.messages) {
      const message = this.state.messages[key];
      const date = ((message.date).slice(0, 16)).replace('T', ' ');

      messages.push(
        <Message
          date={date}
          title={message.title}
          click={this.readMore}
          key={key}
          id={key}
        />
      )
    }

    return (
      <div className="Blog">
        <div className="Message">
          {messages}
        </div>
      </div>
    );
  }
}

export default Blog;