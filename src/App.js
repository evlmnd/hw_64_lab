import React, {Component} from 'react';
import Blog from "./containers/Blog/Blog";
import AddMessage from "./containers/AddMessage/AddMessage";

import './App.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import FullMessage from "./containers/FullMessage/FullMessage";
import EditMessage from "./containers/EditMessage/EditMessage";
import NavBar from "./components/NavBar/NavBar";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <NavBar/>
          <Switch>
            <Route path="/" exact component={Blog}/>
            <Route path="/about" render={() => <div>About</div>}/>
            <Route path="/contacts" render={() => <div>Contacts</div>}/>
            <Route path="/addmessage" component={AddMessage}/>
            <Route path="/messages/:id" exact component={FullMessage}/>
            <Route path="/messages/:id/edit" component={EditMessage}/>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
