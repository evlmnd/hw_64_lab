import axios from 'axios';


const instance = axios.create({
    baseURL: 'https://hw-64-lab.firebaseio.com/'
});

export default instance;