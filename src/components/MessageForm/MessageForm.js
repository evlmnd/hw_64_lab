import React, {Component} from 'react';
import axios from "../../axios-blog";

class MessageForm extends Component {

  state = {
    title: '',
    description: ''
  };

  componentDidMount() {
    if(this.props.messageId) {
      axios.get('messages/' + this.props.messageId + '.json').then(response => {
        this.setState(response.data);
      }).catch(error => {
        console.log(error);
      });
    }
  }

  changeTitle = event => {
    const title = event.target.value;
    this.setState({title});
  };

  changeDescription = event => {
    const description = event.target.value;
    this.setState({description});
  };

  submitHandler = event => {
    event.preventDefault();
    const message = {...this.state};
    message.date = new Date();
    this.props.submit(message);
  };

  render() {
    let form = (
      <form onSubmit={this.submitHandler}>
        <label htmlFor="title">Title</label>
        <input
          type="text" name="title"
          onChange={this.changeTitle} value={this.state.title}
        />
        <label htmlFor="description">Description</label>
        <textarea
          name="description" cols="30" rows="10"
          onChange={this.changeDescription} value={this.state.description}
        />
        <button>Save</button>
      </form>
    );

    return (
      <div className="Add-Message">
        <h4>{this.props.heading}</h4>
        {form}
      </div>
    );
  }
}

export default MessageForm;