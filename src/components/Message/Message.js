import React, {Component} from 'react';
import './Message.css';

class Message extends Component {
    render() {
        return (
            <div>
                <p>{this.props.date}</p>
                <h5>{this.props.title}</h5>
                <button onClick={this.props.click} id={this.props.id}>Read more >></button>
            </div>
        );
    }
}

export default Message;