import React, {Component} from 'react';

import './NavBar.css';
import NavLink from "react-router-dom/es/NavLink";

class NavBar extends Component {
    render() {
        return (
            <div className="Nav-Bar">
                <ul>
                    <li><NavLink to="/">Home</NavLink></li>
                    <li><NavLink to="/addmessage">Add Message</NavLink></li>
                    <li><NavLink to="/about">About</NavLink></li>
                    <li><NavLink to="/contacts">Contacts</NavLink></li>
                </ul>
            </div>
        );
    }
}

export default NavBar;